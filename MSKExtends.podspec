Pod::Spec.new do |s|
  s.name         = "MSKExtends"
  s.version      = "0.2.10"
  s.summary      = "MSK exnted classes"
  s.description  = "MSK exnted classes..."
  s.homepage     = "http://www.littlegleam.com"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'Masaki Oshikawa' => 'oshikawakanu+bitbucket@gmail.com' }
  s.source       = { :git => "https://moshikawa@bitbucket.org/moshikawa/mskextends.git", :tag => s.version.to_s }
  s.platform     = :ios
  s.source_files = 'Classes/*.{h,m}'
#  s.framework    = "libiconv"
  s.library   = "iconv"
  s.requires_arc = true
end

