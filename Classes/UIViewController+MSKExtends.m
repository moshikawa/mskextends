//
//  UIViewController+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UIViewController+MSKExtends.h"

@implementation UIViewController (MSKExtends)

+ (id)msk_viewControllerFromStoryboardIdentifier:(NSString *)identifier {
	UIStoryboard *sb = [UIStoryboard storyboardWithName:NSStringFromClass([self class])
												 bundle:nil];
	UIViewController *result = nil;
	if (identifier == nil) {
		result = [sb instantiateInitialViewController];
	} else {
		result = [sb instantiateViewControllerWithIdentifier:identifier];
	}
	NSAssert(result != nil, @"vc is null");
	return result;
}

+ (id)msk_viewControllerFromStoryboard {
	return [self msk_viewControllerFromStoryboardIdentifier:nil];
}


+ (instancetype)msk_viewControllerFromNib {
	return [[self alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
}

- (void)msk_presentError:(NSError *)error handler:(void (^)())handler {
	error = [self msk_willReceiveError:error];
	if (error != nil) {
		[self msk_didReceiveError:error handler:handler];
	}
}

- (NSError *)msk_willReceiveError:(NSError *)error {
	return error;
}

- (void)msk_didReceiveError:(NSError *)error handler:(void (^)())handler {
	UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error"
																message:error.localizedDescription
														 preferredStyle:UIAlertControllerStyleAlert];
	[ac addAction:[UIAlertAction actionWithTitle:@"Cancel"
										   style:UIAlertActionStyleCancel
										 handler:^(UIAlertAction *action) {
											 if (handler != nil) {
												 handler();
											 }
										 }]];
	[self presentViewController:ac animated:YES completion:nil];
}

- (void)msk_registerNotifications {
}
- (void)msk_unregisterNotifications {
}

- (void)msk_updateNavigationBar:(BOOL)animated {
}

- (BOOL)msk_isVisible {
	return (self.isViewLoaded && self.view.window != nil);
}

- (void)msk_releaseViewObjects {
}

@end
