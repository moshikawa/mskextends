//
//  NSString+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MSKExtends)

- (NSString *)msk_stringByURLEncoding;
- (NSString *)msk_stringByURLDecoding;

- (NSString *)msk_stringBySqliteEscapeString;
- (NSString *)msk_stringBySqliteEscapeStringForLike;

/// @todo 正規表現用に文字列をエスケープ
- (NSString *)msk_regQuotedString;

- (NSString *)msk_stringByRTrimmingCharactersInSet: (NSCharacterSet*)aSet;
- (NSString *)msk_stringByLTrimmingCharactersInSet: (NSCharacterSet*)aSet;

- (NSString *)msk_rot13;

/*
- (NSString *)msk_stringByConvertingHTMLToPlainText;
- (NSString *)msk_stringByDecodingHTMLEntities;
 */

@end
