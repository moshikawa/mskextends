//
//  NSNumber+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (MSKExtends)

- (NSDate *)msk_dateValue;
/// "," 区切りの数値
- (NSString *)msk_formattedStringValue;

@end
