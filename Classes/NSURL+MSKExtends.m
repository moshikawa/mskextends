//
//  NSURL+ParseURL.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "NSURL+MSKExtends.h"

@implementation NSURL (MSKParseURL)

+ (NSDictionary *)msk_parsedQueryDictionaryWithQuery:(NSString *)query usingEncoding:(NSStringEncoding)encoding {
	if (!query) {
		return nil;
	}
	
	NSMutableDictionary *result = [NSMutableDictionary dictionary];
	NSArray *pairs = [query componentsSeparatedByString:@"&"];
	
	for (NSString *kvp in pairs) {
		if ([kvp length] == 0) {
			continue;
		}
		
		NSRange pos = [kvp rangeOfString:@"="];
		NSString *key;
		NSString *val;
		
		if (pos.location == NSNotFound) {
			key = [self _stringByUnescapingFromURLQuery:kvp encoding:encoding];
			val = @"";
		} else {
			key = [self _stringByUnescapingFromURLQuery:[kvp substringToIndex:pos.location] encoding:encoding];
			val = [self _stringByUnescapingFromURLQuery:[kvp substringFromIndex:pos.location + pos.length] encoding:encoding];
		}
		
		if (!key || !val) {
			NSAssert3(0, @"invalid params: %@, %@, %@", key, val, kvp);
			continue; // I'm sure this will bite my arse one day
		}
		
		[result setObject:val forKey:key];
	}
	return result;
}

+ (NSString *)_stringByUnescapingFromURLQuery:(NSString *)query encoding:(NSStringEncoding)encoding {
	if (query == nil) return nil;
	
	NSString *result = nil;
	NSString *deplussed = [query stringByReplacingOccurrencesOfString:@"+" withString:@" "];
	
	NSArray *encodings;
	switch (encoding) {
		case NSUTF8StringEncoding:
			encodings = [[NSArray alloc] initWithObjects:
						  [NSNumber numberWithInt:NSUTF8StringEncoding],
						  [NSNumber numberWithInt:NSShiftJISStringEncoding],
						  [NSNumber numberWithInt:NSJapaneseEUCStringEncoding],
						  [NSNumber numberWithInt:NSISO2022JPStringEncoding],
						  [NSNumber numberWithInt:NSASCIIStringEncoding],
						  nil];
			break;
		case NSJapaneseEUCStringEncoding:
			encodings = [[NSArray alloc] initWithObjects:
						  [NSNumber numberWithInt:NSJapaneseEUCStringEncoding],
						  [NSNumber numberWithInt:NSUTF8StringEncoding],
						  [NSNumber numberWithInt:NSShiftJISStringEncoding],
						  [NSNumber numberWithInt:NSISO2022JPStringEncoding],
						  [NSNumber numberWithInt:NSASCIIStringEncoding],
						  nil];
			break;
		default: // SJIS優先
			encodings = [[NSArray alloc] initWithObjects:
						  [NSNumber numberWithInt:NSShiftJISStringEncoding],
						  [NSNumber numberWithInt:NSUTF8StringEncoding],
						  [NSNumber numberWithInt:NSJapaneseEUCStringEncoding],
						  [NSNumber numberWithInt:NSISO2022JPStringEncoding],
						  [NSNumber numberWithInt:NSASCIIStringEncoding],
						  nil];
			break;
	}
	
	for (NSNumber *num in encodings) {
		NSStringEncoding enc = [num integerValue];
		result = [deplussed stringByReplacingPercentEscapesUsingEncoding:enc];
		if (result != nil) break;
	}
	
	return result;
}


- (NSDictionary *)msk_parsedQueryDictionaryWithUsingEncoding:(NSStringEncoding)encoding {
	NSString *query = self.query;
	if (query == nil && [self.scheme isEqualToString:@"mailto"]) {
		NSArray *arr = [self.absoluteString componentsSeparatedByString:@"?"];
		NSString *toField = nil;
		if (arr.count > 1) {
			query = [arr objectAtIndex:1];
		}
		
		if (arr.count > 0) {
			toField = [arr objectAtIndex:0];
			toField = [toField substringWithRange:NSMakeRange(7, toField.length-7)];
			toField = [toField stringByTrimmingCharactersInSet:
					   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
			if (toField.length > 0) {
				if (query == nil) {
					query = [NSString stringWithFormat:@"to=%@", toField];
				} else {
					query = [NSString stringWithFormat:@"%@&to=%@", query, toField];
				}
			}
		}
	}
	return [self.class msk_parsedQueryDictionaryWithQuery:query usingEncoding:encoding];
}

@end
