//
//  UIBarButtonItem+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

/// UIBarButtonSystemItemの拡張
typedef NS_ENUM(NSInteger, UIBarButtonSystemItemExt) {
	UIBarButtonSystemItemExtConpus = 100,
	UIBarButtonSystemItemExtPrevious,
	UIBarButtonSystemItemExtNext,
	UIBarButtonSystemItemExtTop,
	UIBarButtonSystemItemExtBottom,
	UIBarButtonSystemItemExtBack,
	UIBarButtonSystemItemExtForward,
	UIBarButtonSystemItemExtLeftArrow,
	UIBarButtonSystemItemExtRightArrow,
	UIBarButtonSystemItemExtPageCurl,
};

@interface UIBarButtonItem (MSKExtends)

+ (UIBarButtonItem *)msk_itemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style
								target:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)msk_itemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style
								target:(id)target action:(SEL)selector;
+ (UIBarButtonItem *)msk_systemItem:(UIBarButtonSystemItem)systemItem target:(id)target
							 action:(SEL)selector;
+ (UIBarButtonItem *)msk_spaceItem;

@end
