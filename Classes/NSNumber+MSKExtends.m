//
//  NSNumber+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "NSNumber+MSKExtends.h"

@implementation NSNumber (MSKExtends)

- (NSDate *)msk_dateValue {
	return [NSDate dateWithTimeIntervalSince1970:[self unsignedIntegerValue]];
}

- (NSString *)msk_formattedStringValue {
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	formatter.numberStyle = NSNumberFormatterDecimalStyle;
	return [formatter stringFromNumber:self];
}

@end
