//
//  UIView+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UIView+MSKExtends.h"

@implementation UIView (MSKExtends)

+ (instancetype)msk_viewFromNib {
	return [[self msk_getNib] instantiateWithOwner:nil options:nil].firstObject;
}

+ (UINib *)msk_getNib {
	return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (instancetype)msk_viewWithFrame:(CGRect)frame backgroundColor:(UIColor *)color {
	UIView *result = [[self alloc] initWithFrame:frame];
	result.backgroundColor = color;
	return result;
}

+ (void)msk_dumpSubViews:(UIView *)view {
#ifdef DEBUG
	NSLog(@"dumpSubviews =============");
	[self msk_dumpSubViews:view withIndent:0];
	NSLog(@"==========================");
#endif
}
+ (void)msk_dumpSubViews:(UIView *)view withIndent:(int)indent {
#ifdef DEBUG
	int ip = indent;
	NSString *sp = @"";
	while (ip-- > 0) {
		sp = [sp stringByAppendingString:@"|  "];
	}
	CGRect r = view.frame;
	NSString *hidden = view.hidden || view.alpha == 0 ? @"-" : @"*";
	NSLog(@"%@+ %@ <%.1f, %.1f, %.1f, %.1f> (%.1f, %@)", sp, [[view class] description],
		r.origin.x, r.origin.y, r.size.width, r.size.height, view.alpha, hidden);
	for (UIView *tv in [view subviews]) {
		[self msk_dumpSubViews:tv withIndent:indent+1];
	}
#endif
}

@end
