//
//  UITableViewController+MSKExtends.h
//	MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewController (MSKExtends)

- (void)msk_updateCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)msk_updateVisibleCells;

@end
