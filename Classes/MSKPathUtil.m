//
//  PathUtil.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "MSKPathUtil.h"
#include <sys/xattr.h>

@implementation MSKPathUtil

+ (NSSet *)pathExtensionSetsWithPrimaryContentType:(NSString *)primaryContentType {
    NSDictionary *contentTypes = [NSDictionary dictionaryWithContentsOfFile:
								  [[NSBundle mainBundle] pathForResource:@"types" ofType:@"plist"]];
	
	NSMutableSet *results = [NSMutableSet set];
    for (NSString *key in [contentTypes allKeys]) {
		NSArray *arr = [key componentsSeparatedByString:@"/"];
		NSString *pc = arr.firstObject;
		if ([pc isEqualToString:primaryContentType]) {
			
			NSArray *fileExtensions = [contentTypes objectForKey:key];
			for (NSString *ext in fileExtensions) {
				[results addObject:ext];
			}
		}
    }

	return results;
}


+ (NSString *)contentTypeByPathExtension:(NSString *)pathExtension {
	
	NSString *filePathExt = pathExtension.lowercaseString;
	
	NSString *contentType = nil;
    NSDictionary *contentTypes = [NSDictionary dictionaryWithContentsOfFile:
								  [[NSBundle mainBundle] pathForResource:@"types" ofType:@"plist"]];
	
    for (NSString *key in [contentTypes allKeys]) {
        NSArray *fileExtensions = [contentTypes objectForKey:key];
        for (NSString *ext in fileExtensions) {
            if ([filePathExt isEqual:ext]) {
                contentType = key;
                break;
            }
        }
        if (contentType != nil) break;
    }
	
	if (contentType == nil) {
		contentType = @"application/octet-stream";
	}
	
	return contentType;

}

// バックアップを取らないフラグを立てるメソッド
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
	const char* filePath = [[URL path] fileSystemRepresentation];
	const char* attrName = "com.apple.MobileBackup";
	
	ssize_t result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
	if (result != -1) {
		// The attribute exists, we need to remove it
		int removeResult = removexattr(filePath, attrName, 0);
		if (removeResult == 0) {
			NSLog(@"Removed extended attribute on file %@", URL);
		}
	}
	
	NSError *error = nil;
	[URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
	return error == nil;
}

// バックアップを取らないフラグを削除するメソッド
+ (BOOL)deleteSkipBackupAttributeToItemAtURL:(NSURL *)URL {
	const char* filePath = [[URL path] fileSystemRepresentation];
	const char* attrName = "com.apple.MobileBackup";
	
	ssize_t result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
	if (result != -1) {
		// The attribute exists, we need to remove it
		int removeResult = removexattr(filePath, attrName, 0);
		if (removeResult == 0) {
			NSLog(@"Removed extended attribute on file %@", URL);
		}
	}
	
	NSError *error = nil;
	[URL setResourceValue:[NSNumber numberWithBool:NO] forKey:NSURLIsExcludedFromBackupKey error:&error];
	return error == nil;
}

// バックアップを取らないフラグを確認するメソッド
+ (BOOL)skipBackupAttributeItemAtURL:(NSURL *)URL {
	const char* filePath = [[URL path] fileSystemRepresentation];
	const char* attrName = "com.apple.MobileBackup";
	ssize_t result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
	if (result != -1) {
		// The attribute exists, we need to remove it
		int removeResult = removexattr(filePath, attrName, 0);
		if (removeResult == 0) {
			NSLog(@"Removed extended attribute on file %@", URL);
		}
	}
	
	NSNumber *num=nil;
	[URL getResourceValue:&num forKey:NSURLIsExcludedFromBackupKey error:nil];
	return [num boolValue]; // フラグが有るならYES、無いならNOが返る
}

+ (NSString *)docDir {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}

+ (NSString *)cacheDir {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}
+ (NSString *)libraryDir {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}
+ (NSString *)appTmpDir {
	NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"app_tmp_dir"];
	[self ensureDirExists:path];
	return path;
}
+ (NSString *)thumbDir {
	NSString *path = [[self appTmpDir] stringByAppendingPathComponent:@"thumbs"];
	[self ensureDirExists:path];
	return path;
}


+ (NSString *)appCacheDir {
	NSString *path = [[self cacheDir] stringByAppendingPathComponent:@"AppCaches"];
	[self ensureDirExists:path];
	return path;
}
+ (NSString *)userDataDir {
	NSString *path = [[self libraryDir] stringByAppendingPathComponent:@"UserDatas"];
	[self ensureDirExists:path];
    //[self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:path]];
	return path;
}

+ (void)ensureDirExists:(NSString *)path {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:path]) {
		return;
	}
	[fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (BOOL)moveFile:(NSString *)fromPath toPath:(NSString *)toPath {
	NSFileManager *fm = [NSFileManager defaultManager];
	if (![fm fileExistsAtPath:fromPath]) return NO;
	
	BOOL isDir = NO;
	if ([fm fileExistsAtPath:toPath isDirectory:&isDir]) {
		if (isDir) {
			[self removeDirectory:toPath];
		} else {
			if (![fm removeItemAtPath:toPath error:nil]) {
				return NO;
			}
		}
	}
	return [fm moveItemAtPath:fromPath toPath:toPath error:nil];
}

+ (BOOL)moveContentsOfDicrectory:(NSString *)fromDirectory toDirectory:(NSString *)toDirectory {
	NSFileManager *fm = [NSFileManager defaultManager];
	
	[self ensureDirExists:toDirectory];
	NSArray *files = [fm contentsOfDirectoryAtPath:fromDirectory error:nil];
	BOOL flag = YES;
	
	for (NSString *fileName in files) {
		NSString *chkPath = [fromDirectory stringByAppendingPathComponent:fileName];
		NSString *toPath = [toDirectory stringByAppendingPathComponent:fileName];
		NSError *e = nil;
		//BOOL isDir = NO;
		if ([fm fileExistsAtPath:toPath]) {
			[fm removeItemAtPath:toPath error:&e];
		}
		
		e = nil;
		if ([fm fileExistsAtPath:chkPath]) {
			[fm moveItemAtPath:chkPath toPath:toPath error:&e];
			if (e != nil) {
				flag = NO;
			}
		}
	}
	return flag;
}

+ (BOOL)removeDirectory:(NSString *)path {
	return [self removeDirectory:path removeSelf:YES];
}

+ (BOOL)removeDirectory:(NSString *)path removeSelf:(BOOL)removeSelf {
	// 子ディレクトリがあれば空にする
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
	BOOL flag = YES;
	if (files != nil) {
		NSEnumerator *en = [files objectEnumerator];
		NSString *fileName;
		while ((fileName = [en nextObject])) {
			NSString *chkPath = [path stringByAppendingPathComponent:fileName];
			NSError *e = nil;
			BOOL isDir = NO;
			if ([[NSFileManager defaultManager] fileExistsAtPath:chkPath isDirectory:&isDir] && isDir) {
				if (![self removeDirectory:chkPath]) flag = NO;
			} else {
			}
			[[NSFileManager defaultManager] removeItemAtPath:chkPath error:&e];
			if (e != nil) {
				flag = NO;
			}
		}
	}
	
	NSError *e = nil;
	if (removeSelf) [[NSFileManager defaultManager] removeItemAtPath:path error:&e];
	return flag;
	
}

@end
