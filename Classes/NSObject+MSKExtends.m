//
//  NSObject+MSKExtends.m
//  MSKExtends
//
//  Created by admin on 2014/11/19.
//  Copyright (c) 2014年 Little Gleam. All rights reserved.
//

#import "NSObject+MSKExtends.h"

@implementation NSObject (MSKExtends)

- (void)_msk_executeBlock:(void (^)(void))block {
	block();
}

- (void)msk_performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay {
	[self performSelector:@selector(_msk_executeBlock:)
			   withObject:[block copy]
			   afterDelay:delay];
}

@end
