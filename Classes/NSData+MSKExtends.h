//
//  NSData+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MSKExtends)

- (NSString *)msk_imageFileExtention;
- (NSString *)msk_stringValue;

@end
