//
//  NSDictionary+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MSKExtends)

/// NSNullを削除したDictionaryを返す
- (id)msk_nullRemovedDictionary;

@end