//
//  MskImage.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "MSKImageUtil.h"
#import <Accelerate/Accelerate.h>

@implementation MSKImageUtil

+ (NSString *)colorStringWithRGB:(NSString *)rgb {
	if ([rgb hasPrefix:@"#"]) {
		rgb = [rgb substringFromIndex:1];
	}
	if ([rgb length] < 6) return nil;
	
	NSString *colorString = [NSString stringWithFormat:
							 @"0x%@ 0x%@ 0x%@", 
							 [rgb substringWithRange:NSMakeRange(0, 2)],
							 [rgb substringWithRange:NSMakeRange(2, 2)],
							 [rgb substringWithRange:NSMakeRange(4, 2)]];
	return colorString;
}

+ (NSString *)RGBStringWithColor:(UIColor *)color {
	if (color == nil) return @"#FFFFFF";
	size_t numComponents = CGColorGetNumberOfComponents(color.CGColor);
	const CGFloat *components = CGColorGetComponents(color.CGColor);
	CGFloat r, g, b;
	if (numComponents == 4) {
		r = components[0];
		g = components[1];
		b = components[2];
	} else {
		r = components[0];
		g = components[0];
		b = components[0];
	}
	return [NSString stringWithFormat:@"#%X%X%X", (int)(r*255), (int)(g*255), (int)(b*255)];
}


- (UIImage *)rotateImage:(UIImage*)img angle:(int)angle
{
    CGImageRef      imgRef = [img CGImage];
    CGContextRef    context;
    
    switch (angle) {
        case 90:
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(img.size.height, img.size.width), YES, img.scale);
            context = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(context, img.size.height, img.size.width);
            CGContextScaleCTM(context, 1, -1);
            CGContextRotateCTM(context, M_PI_2);
            break;
        case 180:
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(img.size.width, img.size.height), YES, img.scale);
            context = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(context, img.size.width, 0);
            CGContextScaleCTM(context, 1, -1);
            CGContextRotateCTM(context, -M_PI);
            break;
        case 270:
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(img.size.height, img.size.width), YES, img.scale);
            context = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(context, 1, -1);
            CGContextRotateCTM(context, -M_PI_2);
            break;
        default:
            return img;
            break;
    }
    
    CGContextDrawImage(context, CGRectMake(0, 0, img.size.width, img.size.height), imgRef);
    UIImage*    result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

+ (UIImage *)removeOrientation:(UIImage *)image {
	if (image.imageOrientation == UIImageOrientationUp) return image;
	CGAffineTransform transform = CGAffineTransformIdentity;
	
	switch (image.imageOrientation) {
		case UIImageOrientationDown:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
			transform = CGAffineTransformTranslate(transform, image.size.width, 0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;
			
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, 0, image.size.height);
			transform = CGAffineTransformRotate(transform, -M_PI_2);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationUpMirrored:
			break;
	}
	
	switch (image.imageOrientation) {
		case UIImageOrientationUpMirrored:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformTranslate(transform, image.size.width, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
			
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformTranslate(transform, image.size.height, 0);
			transform = CGAffineTransformScale(transform, -1, 1);
			break;
		case UIImageOrientationUp:
		case UIImageOrientationDown:
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
			break;
	}
	
	// Now we draw the underlying CGImage into a new context, applying the transform
	// calculated above.
	CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
											 CGImageGetBitsPerComponent(image.CGImage), 0,
											 CGImageGetColorSpace(image.CGImage),
											 CGImageGetBitmapInfo(image.CGImage));
	CGContextConcatCTM(ctx, transform);
	switch (image.imageOrientation) {
		case UIImageOrientationLeft:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRight:
		case UIImageOrientationRightMirrored:
			// Grr...
			CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
			break;
			
		default:
			CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
			break;
	}
	
	// And now we just create a new UIImage from the drawing context
	CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
	UIImage *img = [UIImage imageWithCGImage:cgimg];
	CGContextRelease(ctx);
	CGImageRelease(cgimg);
	return img;
}

+ (UIColor *)colorWithHex:(NSString*)hexColor {
	UIColor *color = [UIColor blackColor];
	
	NSString *colorString = [self colorStringWithRGB:hexColor];
	if (colorString == nil) return color;
	
	unsigned red, green, blue;
	NSScanner *scanner = [NSScanner scannerWithString:colorString];
	if ([scanner scanHexInt:&red] && 
		[scanner scanHexInt:&green] &&
		[scanner scanHexInt:&blue]) {
		NSLog(@"rgb %f %f %f", (float)red, (float)green, (float)blue);
		color = [UIColor colorWithRed:(float)red/255.0 green:(float)green/255.0 blue:(float)blue/255.0 alpha:1.0];
	}
	return color;
}


+ (UIImage *)colorImageWithRGB:(NSString *)rgb {
	const int height = 20;
	const int width = 30;
	CGContextRef    context = NULL;
	
	CGColorSpaceRef  imageColorSpace = CGColorSpaceCreateDeviceRGB();
	
	context = CGBitmapContextCreate(NULL, width, height,
									 8, width * 4,
									 imageColorSpace, (kCGBitmapAlphaInfoMask|kCGBitmapFloatComponents));
	
	NSString *colorString = [self colorStringWithRGB:rgb];
	if (colorString == nil) {
		CGColorSpaceRelease(imageColorSpace);
		CGContextRelease(context);
		return nil;
	}
	
	unsigned red, green, blue;
	float r, g, b;
	r = g = b = 0.0f;
	NSScanner *scanner = [NSScanner scannerWithString:colorString];
	if ([scanner scanHexInt:&red] && 
		[scanner scanHexInt:&green] &&
		[scanner scanHexInt:&blue]) {
		r = (float)red/255.0f;
		g = (float)green/255.0f;
		b = (float)blue/255.0f;
	}
	
	CGContextSetRGBFillColor (context, r, g, b, 1);
	CGContextFillRect (context, CGRectMake (0, 0, width, height));
	
//	CGContextSetRGBFillColor(context, 0, 0, 1, 1);
//	CGContextFillRect (context, CGRectMake (80, 80, 40, 30));
	
	CGImageRef image = CGBitmapContextCreateImage(context);
	
	UIImage *result = [UIImage imageWithCGImage:image];
	
	CGColorSpaceRelease( imageColorSpace );
	CGContextRelease(context);
	CGImageRelease( image );
	
	return result;
}


+ (UIImage *)cropImage:(UIImage *)image withSize:(CGSize)size {
	
	float w = image.size.width;
	float h = image.size.height;
	
	CGRect r = CGRectZero;
	
	float wp = size.width / w;
	float hp = size.height / h;
	
	float per = MAX(wp, hp);
	
	r.size.width = ceil(w * per);
	r.size.height = ceil(h * per);
	r.origin.x = (int)((size.width - r.size.width) / 2);
	r.origin.y = (int)((size.height - r.size.height) / 2);
	
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
	[image drawInRect:r];
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return img;
}



+ (UIImage *)blurImage:(UIImage *)image withBlurLevel:(CGFloat)blur {
    if ((blur < 0.0f) || (blur > 1.0f)) {
        blur = 0.5f;
    }
    
    int boxSize = (int)(blur * 200);
    boxSize -= (boxSize % 2) + 1;
    
    CGImageRef img = image.CGImage;
    
    vImage_Buffer inBuffer, outBuffer;
    vImage_Error error;
    void *pixelBuffer;
    
    CGDataProviderRef inProvider = CGImageGetDataProvider(img);
    CFDataRef inBitmapData = CGDataProviderCopyData(inProvider);
    
    inBuffer.width = CGImageGetWidth(img);
    inBuffer.height = CGImageGetHeight(img);
    inBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    inBuffer.data = (void*)CFDataGetBytePtr(inBitmapData);
    
    pixelBuffer = malloc(CGImageGetBytesPerRow(img) * CGImageGetHeight(img));
	
    outBuffer.data = pixelBuffer;
    outBuffer.width = CGImageGetWidth(img);
    outBuffer.height = CGImageGetHeight(img);
    outBuffer.rowBytes = CGImageGetBytesPerRow(img);
    
    error = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, NULL,
                                       0, 0, boxSize, boxSize, NULL,
                                       kvImageEdgeExtend);
    
    if (error) {
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(
                                             outBuffer.data,
                                             outBuffer.width,
                                             outBuffer.height,
                                             8,
                                             outBuffer.rowBytes,
                                             colorSpace,
                                             CGImageGetBitmapInfo(image.CGImage));
    
    CGImageRef imageRef = CGBitmapContextCreateImage (ctx);
    UIImage *returnImage = [UIImage imageWithCGImage:imageRef];
    
    //clean up
    CGContextRelease(ctx);
    
    free(pixelBuffer);
    CFRelease(inBitmapData);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(imageRef);
    
    return returnImage;
}

// 画像リサイズ
+ (UIImage *)resizeImage:(UIImage *)image targetSize:(CGSize)size {
	/// @todo いずれAspectFit等に対応させたい CGRect frame = AVMakeRectWithAspectRatioInsideRect(image.size, self.imageView.bounds);
	if (CGSizeEqualToSize(size, image.size)) return image;
	CGRect r = CGRectMake(0, 0, size.width, size.height);
	
	// リサイズした画像をリセット
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
	[image drawInRect:r];
	UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return resultImage;
}



@end
