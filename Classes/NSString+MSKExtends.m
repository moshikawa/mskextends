//
//  NSString+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "NSString+MSKExtends.h"
//#import "GTMNSString+HTML.h"

@implementation NSString (MSKExtends)

- (NSString *)msk_stringByURLEncoding {
	return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
}

- (NSString *)msk_stringByURLDecoding {
	return [self stringByRemovingPercentEncoding];
}

- (NSString *)msk_regQuotedString {
	return [self copy];
	/*
	NSMutableString *escapedString = [[self mutableCopy] autorelease];
	
	NSArray *escapeChars = [NSArray arrayWithObjects:
							@".", @"\\", @"+", @"*", @"?", @"[", @"^", @"]", @"$", @"(", @")",
							@"{", @"}", @"=", @"!", @"<", @">", @"|", @":", @"-",
							nil];
	
	NSArray *replaceChars = [NSArray arrayWithObjects:
							 @"\\.", @"\\\\", @"\\+", @"\\*", @"\\?", @"\\[", @"\\^", @"\\]", @"\\$", @"\\(", @"\\)",
							 @"\\{", @"\\}", @"\\=", @"\\!", @"\\<", @"\\>", @"\\|", @"\\:", @"\\-",
							 nil];
	
	for(int i=0; i<[escapeChars count]; i++) {
		[escapedString replaceOccurrencesOfString:[escapeChars objectAtIndex:i]
									   withString:[replaceChars objectAtIndex:i]
										  options:NSLiteralSearch
											range:NSMakeRange(0, [escapedString length])];
	}
	
	return [NSString stringWithString:escapedString];
	 */
}


- (NSString *)msk_stringBySqliteEscapeString {
	return [self stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
}

- (NSString *)msk_stringBySqliteEscapeStringForLike {
	NSMutableString *escapedString = [self mutableCopy];
	
	NSArray *escapeChars = [NSArray arrayWithObjects:
							@"$", @"'", @"%", @"_",
							nil];
	
	NSArray *replaceChars = [NSArray arrayWithObjects:
							 @"$$", @"''", @"$%", @"$_",
							 nil];
	
	for(int i=0; i<[escapeChars count]; i++) {
		[escapedString replaceOccurrencesOfString:[escapeChars objectAtIndex:i]
									   withString:[replaceChars objectAtIndex:i]
										  options:NSLiteralSearch
											range:NSMakeRange(0, [escapedString length])];
	}
	
	return [NSString stringWithString:escapedString];
}


- (NSString *)msk_stringByLTrimmingCharactersInSet:(NSCharacterSet*)aSet {
	NSUInteger end = [self length];
	NSUInteger start  = 0;
	if (aSet == nil){
		[NSException raise: NSInvalidArgumentException
					format: @"%@ - nil character set argument", NSStringFromSelector(_cmd)];
	}
	
	if (end == 0) {
		return self;
	}
	
	while (start < end) {
		unichar letter = [self characterAtIndex:start];
		if (![aSet characterIsMember:letter]) {
			break;
		}
		start++;
	}
	
	if (start == end) {
		return @"";
	}
	
	return [self substringWithRange:NSMakeRange(start, end - start)];
}

- (NSString *)msk_stringByRTrimmingCharactersInSet:(NSCharacterSet*)aSet {
	NSUInteger end = [self length];
	if (aSet == nil){
		[NSException raise: NSInvalidArgumentException
					format: @"%@ - nil character set argument", NSStringFromSelector(_cmd)];
	}
	
	if (end == 0) {
		return self;
	}
	
	while (end > 0) {
		unichar letter = [self characterAtIndex:end - 1];
		if (![aSet characterIsMember:letter]) {
			break;
		}
		end--;
	}
	
	if (end == 0) {
		return @"";
	}
	
	return [self substringWithRange:NSMakeRange(0, end)];
}

- (NSString *)msk_rot13 {
	NSString *aString = [self copy];
	NSString *newString;
    NSUInteger length;
    unichar *buf;
    NSUInteger i;
	
    length = [aString length];
    buf = malloc( (length + 1) * sizeof(unichar) );
    [aString getCharacters:buf];
    buf[length] = (unichar)0; // not really needed....
	
    for (i = 0; i < length; i++)
	{
        if (buf[i] >= (unichar)'a' && buf[i] <= (unichar) 'z')
		{
            buf[i] += 13;
            if (buf[i] > 'z') buf[i] -= 26;
        }
		else if (buf[i] >= (unichar)'A' && buf[i] <= (unichar) 'Z')
		{
            buf[i] += 13;
            if (buf[i] > 'Z') buf[i] -= 26;
        }
    }
	
    newString = [NSString stringWithCharacters:buf length:length];
    free(buf);
    return newString;
}

/*
- (NSString *)msk_stringByConvertingHTMLToPlainText {
	
	// Character sets
	NSCharacterSet *stopCharacters = [NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"< \t\n\r%C%C%C%C", 0x0085, 0x000C, 0x2028, 0x2029]];
	NSCharacterSet *newLineAndWhitespaceCharacters = [NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@" \t\n\r%C%C%C%C", 0x0085, 0x000C, 0x2028, 0x2029]];
	NSCharacterSet *tagNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
	
	// Scan and find all tags
	NSMutableString *result = [[NSMutableString alloc] initWithCapacity:self.length];
	NSScanner *scanner = [[NSScanner alloc] initWithString:self];
	[scanner setCharactersToBeSkipped:nil];
	[scanner setCaseSensitive:YES];
	NSString *str = nil, *tagName = nil;
	BOOL dontReplaceTagWithSpace = NO;
	do {
		
		// Scan up to the start of a tag or whitespace
		if ([scanner scanUpToCharactersFromSet:stopCharacters intoString:&str]) {
			[result appendString:str];
			str = nil; // reset
		}
		
		// Check if we've stopped at a tag/comment or whitespace
		if ([scanner scanString:@"<" intoString:NULL]) {
			
			// Stopped at a comment or tag
			if ([scanner scanString:@"!--" intoString:NULL]) {
				
				// Comment
				[scanner scanUpToString:@"-->" intoString:NULL];
				[scanner scanString:@"-->" intoString:NULL];
				
			} else {
				
				// Tag - remove and replace with space unless it's
				// a closing inline tag then dont replace with a space
				if ([scanner scanString:@"/" intoString:NULL]) {
					
					// Closing tag - replace with space unless it's inline
					tagName = nil; dontReplaceTagWithSpace = NO;
					if ([scanner scanCharactersFromSet:tagNameCharacters intoString:&tagName]) {
						tagName = [tagName lowercaseString];
						dontReplaceTagWithSpace = ([tagName isEqualToString:@"a"] ||
												   [tagName isEqualToString:@"b"] ||
												   [tagName isEqualToString:@"i"] ||
												   [tagName isEqualToString:@"q"] ||
												   [tagName isEqualToString:@"span"] ||
												   [tagName isEqualToString:@"em"] ||
												   [tagName isEqualToString:@"strong"] ||
												   [tagName isEqualToString:@"cite"] ||
												   [tagName isEqualToString:@"abbr"] ||
												   [tagName isEqualToString:@"acronym"] ||
												   [tagName isEqualToString:@"label"]);
					}
					
					// Replace tag with string unless it was an inline
					if (!dontReplaceTagWithSpace && result.length > 0 && ![scanner isAtEnd]) [result appendString:@" "];
					
				}
				
				// Scan past tag
				[scanner scanUpToString:@">" intoString:NULL];
				[scanner scanString:@">" intoString:NULL];
				
			}
			
		} else {
			
			// Stopped at whitespace - replace all whitespace and newlines with a space
			if ([scanner scanCharactersFromSet:newLineAndWhitespaceCharacters intoString:NULL]) {
				if (result.length > 0 && ![scanner isAtEnd]) [result appendString:@" "]; // Dont append space to beginning or end of result
			}
			
		}
		
	} while (![scanner isAtEnd]);
	
	// Decode HTML entities and return
	return [result msk_stringByDecodingHTMLEntities];
	
}

- (NSString *)msk_stringByDecodingHTMLEntities {
    return [NSString stringWithString:[self gtm_stringByUnescapingFromHTML]];
}
*/

@end
