//
//  NSObject+MSKExtends.h
//  MSKExtends
//
//  Created by admin on 2014/11/19.
//  Copyright (c) 2014年 Little Gleam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (MSKExtends)

- (void)msk_performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

@end
