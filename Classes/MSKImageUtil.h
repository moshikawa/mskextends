//
//  MskImage.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSKImageUtil : NSObject

+ (NSString *)colorStringWithRGB:(NSString *)rgb;
+ (NSString *)RGBStringWithColor:(UIColor *)color;
+ (UIColor *)colorWithHex:(NSString*)hexColor;
+ (UIImage *)colorImageWithRGB:(NSString *)rgb;
+ (UIImage *)cropImage:(UIImage *)image withSize:(CGSize)size;
+ (UIImage *)resizeImage:(UIImage *)image targetSize:(CGSize)size;

/// 回転を削除
+ (UIImage *)removeOrientation:(UIImage *)image;
/// 回転する
- (UIImage*)rotateImage:(UIImage*)img angle:(int)angle;
+ (UIImage *)blurImage:(UIImage *)image withBlurLevel:(CGFloat)blur;

@end
