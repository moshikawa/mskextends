//
//  NSURL+ParseURL.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (MSKParseURL)

/// NSURLのQueryを解析してDictionaryで返す encodingで優先するエンコーディングを指定可能。
+ (NSDictionary *)msk_parsedQueryDictionaryWithQuery:(NSString *)query usingEncoding:(NSStringEncoding)encoding;
/// NSURLのQueryを解析してDictionaryで返す encodingで優先するエンコーディングを指定可能。
- (NSMutableDictionary *)msk_parsedQueryDictionaryWithUsingEncoding:(NSStringEncoding)encoding;

@end
