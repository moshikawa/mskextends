//
//  NSDictionary+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "NSDictionary+MSKExtends.h"

@implementation NSDictionary (MSKExtends)

- (id)msk_nullRemovedDictionary {
	NSMutableDictionary *result = [NSMutableDictionary dictionary];
	for (NSString *key in self.allKeys) {
		NSObject *item = [self objectForKey:key];
		if (item == [NSNull null]) continue;
		[result setObject:item forKey:key];
	}
	return result;
}

@end
