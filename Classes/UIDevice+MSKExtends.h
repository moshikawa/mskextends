//
//  UIApplication+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (MSKExtends)

/// バージョン比較
- (BOOL)msk_isOSGraterThan:(NSString *)version;
- (BOOL)msk_isOSMoreThan:(NSString *)version;

/// OSの名前
- (NSString*)msk_systemName;
/// MACアドレス
- (NSString *)msk_macAddress;
/// キャリア名
- (NSString *)msk_carrierName;

@end
