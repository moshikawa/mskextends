//
//  UIApplication+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UIDevice+MSKExtends.h"

#import <sys/utsname.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#import <objc/runtime.h>
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@implementation UIDevice (MSKExtends)

- (NSString*)msk_systemName {
	struct utsname u;
	uname(&u);
	return [NSString stringWithCString:u.machine encoding:NSUTF8StringEncoding];
}

- (NSString *)msk_macAddress {
	int                 mib[6];
	size_t              len;
	char                *buf;
	unsigned char       *ptr;
	struct if_msghdr    *ifm;
	struct sockaddr_dl  *sdl;
	
	mib[0] = CTL_NET;
	mib[1] = AF_ROUTE;
	mib[2] = 0;
	mib[3] = AF_LINK;
	mib[4] = NET_RT_IFLIST;
	
	if ((mib[5] = if_nametoindex("en0")) == 0) {
		printf("Error: if_nametoindex error\n");
		return nil;
	}
	
	if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
		printf("Error: sysctl, take 1\n");
		return nil;
	}
	
	if ((buf = malloc(len)) == NULL) {
		printf("Could not allocate memory. error!\n");
		return nil;
	}
	
	if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
		printf("Error: sysctl, take 2");
		free(buf);
		return nil;
	}
	
	ifm = (struct if_msghdr *)buf;
	sdl = (struct sockaddr_dl *)(ifm + 1);
	ptr = (unsigned char *)LLADDR(sdl);
	NSString *result = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
						*ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
	free(buf);
	return result;
}

- (NSString *)msk_carrierName {
	if (![CTTelephonyNetworkInfo class]) return @"undefined";
	
	CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [netinfo subscriberCellularProvider];
	NSLog(@"Carrier Name: %@", carrier.carrierName);
	if (carrier.carrierName == nil) return @"";
	return carrier.carrierName;
}

- (BOOL)msk_isOSGraterThan:(NSString *)version {
	return ([version compare:[self systemVersion] options:NSNumericSearch] == NSOrderedAscending);
}

- (BOOL)msk_isOSMoreThan:(NSString *)version {
	NSComparisonResult od = [version compare:[self systemVersion] options:NSNumericSearch];
	return (od == NSOrderedAscending || od == NSOrderedSame);
}

@end
