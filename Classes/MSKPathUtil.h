//
//  PathUtil.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class     PathUtil
 @brief		パス関連のクラス
 
 <p>
 
 @author	Masaki Oshikawa
 @since		1.0
 */
@interface MSKPathUtil : NSObject {
	
}

/// image -> jpg, jpeg, gif...
+ (NSSet *)pathExtensionSetsWithPrimaryContentType:(NSString *)primaryContentType;

+ (NSString *)contentTypeByPathExtension:(NSString *)pathExtension;

// バックアップ対象から外すフラグを付ける
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (BOOL)deleteSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (BOOL)skipBackupAttributeItemAtURL:(NSURL *)URL;

+ (NSString *)docDir;
+ (NSString *)cacheDir;
+ (NSString *)libraryDir;

/// アプリ用TMPディレクトリ
+ (NSString *)appTmpDir;
/// キャッシュ
+ (NSString *)appCacheDir;
/// documents以外のファイル
+ (NSString *)userDataDir;

+ (NSString *)thumbDir;

// 存在しなければディレクトリ作成
+ (void)ensureDirExists:(NSString*)path;

// ファイル移動(上書き)
+ (BOOL)moveFile:(NSString *)fromPath toPath:(NSString *)toPath;

// fromDirectoryの中身をtoDirectoryへ移動
+ (BOOL)moveContentsOfDicrectory:(NSString *)fromDirectory toDirectory:(NSString *)toDirectory;
// フォルダ削除
+ (BOOL)removeDirectory:(NSString*)path;
// removeSelf = pathのディレクトリ自身も削除する
+ (BOOL)removeDirectory:(NSString*)path removeSelf:(BOOL)removeSelf;

@end
