//
//  UITableViewController+MSKExtends.m
//	MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UITableViewController+MSKExtends.h"

@implementation UITableViewController (MSKExtends)

- (void)msk_updateCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
}
- (void)msk_updateVisibleCells {
	for (UITableViewCell *cell in self.tableView.visibleCells) {
		NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
		[self msk_updateCell:cell atIndexPath:indexPath];
	}
}

@end
