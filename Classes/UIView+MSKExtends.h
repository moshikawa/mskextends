//
//  UIView+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MSKExtends)

/// 同じ名前のxibからファイル作成
+ (instancetype)msk_viewFromNib;
+ (UINib *)msk_getNib;
+ (void)msk_dumpSubViews:(UIView *)view;
+ (instancetype)msk_viewWithFrame:(CGRect)frame backgroundColor:(UIColor *)color;

@end
