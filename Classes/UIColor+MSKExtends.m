//
//  UIColor+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UIColor+MSKExtends.h"

@implementation UIColor (MSKExtends)

+ (UIColor *)msk_baseColor {
	return [self msk_defaultBackgroundColor];
}

+ (UIColor *)msk_accentColor {
	return [UIColor redColor];
}

+ (UIColor *)msk_assortColor {
	return [UIColor colorWithRed:0.0f green:0.46 blue:1.0f alpha:1.0f];
}

+ (UIColor *)msk_defaultBackgroundColor {
	return [self msk_lightBackgroundColor];
}
+ (UIColor *)msk_lightBackgroundColor {
	return [UIColor colorWithWhite:0.96f alpha:1.0f];
}
+ (UIColor *)msk_darkBackgroundColor {
	return [UIColor colorWithWhite:0.4f alpha:1.0f];
}

+ (UIColor *)msk_defaultTextColor {
	return [UIColor darkGrayColor];
}
+ (UIColor *)msk_lightTextColor {
	return [UIColor lightGrayColor];
}
+ (UIColor *)msk_darkTextColor {
	return [UIColor blackColor];
}

@end
