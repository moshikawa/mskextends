//
//  NSData+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "NSData+MSKExtends.h"

@implementation NSData (MSKExtends)

- (NSString *)msk_imageFileExtention {
    uint8_t c;
	if (self.length > 0) {
		[self getBytes:&c length:1];
		
		switch (c) {
			case 0xFF:
				return @"jpg";
			case 0x89:
				return @"png";
			case 0x47:
				return @"gif";
			case 0x49:
			case 0x4D:
				return @"tiff";
		}
	}
    return nil;
}

- (NSString *)msk_stringValue {
	return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}

@end
