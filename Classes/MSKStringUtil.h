//
//  MskStringUtil.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MSKStringUtil : NSObject {

}

+ (NSString *)stringByUsingIconvWithData:(NSData *)data encoding:(NSStringEncoding)encoding;
+ (NSString *)MD5String:(NSString *)string;
+ (NSString *)sha1String:(NSString *)string;
+ (NSString *)sha1StringWithData:(NSData *)data;
+ (NSString *)separateWithComma:(NSInteger)integer;
+ (NSString*)trim:(NSString*)s;
+ (NSString*)uuid;

@end
