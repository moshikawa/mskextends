//
//  UIColor+MSKExtends.h
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MSKExtends)

//+ (void)msk_setColors:(NSArray *)colors;

+ (UIColor *)msk_baseColor;
+ (UIColor *)msk_accentColor;
+ (UIColor *)msk_assortColor;

+ (UIColor *)msk_defaultBackgroundColor;
+ (UIColor *)msk_lightBackgroundColor;
+ (UIColor *)msk_darkBackgroundColor;

+ (UIColor *)msk_defaultTextColor;
+ (UIColor *)msk_lightTextColor;
+ (UIColor *)msk_darkTextColor;

@end
