//
//  UIBarButtonItem+MSKExtends.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "UIBarButtonItem+MSKExtends.h"

@implementation UIBarButtonItem (MSKExtends)

+ (UIBarButtonItem *)msk_itemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style
								target:(id)target action:(SEL)selector {
	return [[UIBarButtonItem alloc] initWithTitle:title style:style target:target action:selector];
	
}

+ (UIBarButtonItem *)msk_itemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style
						target:(id)target action:(SEL)selector {
	return [[UIBarButtonItem alloc] initWithImage:image style:style target:target action:selector];
	
}

+ (UIBarButtonItem *)msk_systemItem:(UIBarButtonSystemItem)systemItem target:(id)target
						 action:(SEL)selector {
	return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:target action:selector];
	
}

+ (UIBarButtonItem *)msk_spaceItem {
	return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
														  target:nil action:nil];
}

@end
