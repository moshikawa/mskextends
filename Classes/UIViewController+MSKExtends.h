//
//  UIViewController+MSKExtends.h
//	MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MSKExtends)

+ (id)msk_viewControllerFromStoryboard;
+ (id)msk_viewControllerFromStoryboardIdentifier:(NSString *)identifier;
+ (instancetype)msk_viewControllerFromNib;

- (BOOL)msk_isVisible;

- (void)msk_updateNavigationBar:(BOOL)animated;

- (void)msk_releaseViewObjects;

/// エラー発生時に呼んでください。
- (void)msk_presentError:(NSError *)error handler:(void (^)())handler;
/// presentErrorで受け取ったエラーの内容を変更したい場合にどうぞ
- (NSError *)msk_willReceiveError:(NSError *)error;
/// presentErrorで受け取ったエラーの処理。デフォルトでアラート表示する
- (void)msk_didReceiveError:(NSError *)error handler:(void (^)())handler;

/// NSNotification等、監視を開始
- (void)msk_registerNotifications;
/// registerNotificationsで登録した監視を解除
- (void)msk_unregisterNotifications;

@end
