//
//  MskStringUtil.m
//  MSKExtends
//
//  Created by Masaki Oshikawa on 2014/11/12
//  Copyright (c) 2012年 Little Gleam All rights reserved.
//

#import "MSKStringUtil.h"
//#import "RegexKitLite.h"
#import <CommonCrypto/CommonDigest.h>
#include <iconv.h>

@implementation MSKStringUtil

/// iconv対応エンコーディングの出力
static int __print_one(unsigned int namescount, const char * const * names,
                      void* data)
{
	unsigned int i;
	(void)data;
	for (i = 0; i < namescount; i++) {
		if (i > 0)
			putc(' ',stdout);
		fputs(names[i],stdout);
	}
	putc('\n',stdout);
	return 0;
}

+ (void)printIconvSupportEncodings {
	iconvlist(__print_one, NULL);
}

+ (NSString *)stringByUsingIconvWithData:(NSData *)data encoding:(NSStringEncoding)encoding {

	char *from = NULL;
	switch (encoding) {
		case NSShiftJISStringEncoding:
			from = "SHIFT_JIS";
			break;
		
		case NSJapaneseEUCStringEncoding:
			from = "EUC-JISX0213";
			break;
			
		case NSUTF8StringEncoding:
			from = "UTF-8";
			break;
			
		case NSASCIIStringEncoding:
			from = "US-ASCII";
			break;
			
		default: // NSASCIIStringEncoding
			NSAssert1(0, @"invalid encoding %ld", (long)encoding);
			from = "US-ASCII";
			break;
	}
		
	iconv_t cd = iconv_open("UTF-8", from); // convert to UTF-8 from UTF-8
	
	int one = 1;
	iconvctl(cd, ICONV_SET_DISCARD_ILSEQ, &one); // discard invalid characters
	
	size_t inbytesleft, outbytesleft, outbyteslength;
	inbytesleft = data.length;
	outbytesleft = outbyteslength = 6 * inbytesleft;
	char *inbuf  = (char *)data.bytes;
	char *outbuf = malloc(sizeof(char) * outbytesleft);
	char *outptr = outbuf;
	while (iconv(cd, &inbuf, &inbytesleft, &outptr, &outbytesleft)
		   == (size_t)-1) {
		NSAssert(0, @"this should not happen, seriously");
		return nil;
	}
	NSString *result = [[NSString alloc] initWithBytes:outbuf
												 length:outbyteslength - outbytesleft
											   encoding:NSUTF8StringEncoding];
	
	iconv_close(cd);
	free(outbuf);
	
	return result;
}

+ (NSString *)MD5String:(NSString *)string
{
	if (string == nil) {
		return nil;
	}
    const char *cStr = [string UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest );
    
    char md5string[CC_MD5_DIGEST_LENGTH*2];
    int i; for (i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        sprintf(md5string+i*2, "%02X", digest[i]);
    }
    //return [NSString stringWithCString:md5string length:CC_MD5_DIGEST_LENGTH*2];
	return [NSString stringWithCString:md5string encoding:NSUTF8StringEncoding];
}

+ (NSString *)sha1String:(NSString *)string
{
	if (string == nil) {
		return nil;
	}
    const char *cStr = [string UTF8String];
    unsigned char digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1( cStr, (CC_LONG)strlen(cStr), digest );
    
    char sha1string[CC_SHA1_DIGEST_LENGTH*2];
    int i; for (i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        sprintf(sha1string+i*2, "%02X", digest[i]);
    }
    //return [NSString stringWithCString:md5string length:CC_MD5_DIGEST_LENGTH*2];
	return [NSString stringWithCString:sha1string encoding:NSUTF8StringEncoding];
	
}

+ (NSString *)sha1StringWithData:(NSData *)data
{
	if (data == nil) {
		return nil;
	}
    unsigned char digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([data bytes], (CC_LONG)[data length], digest );
    
    char sha1string[CC_SHA1_DIGEST_LENGTH*2];
    int i; for (i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        sprintf(sha1string+i*2, "%02X", digest[i]);
    }
    //return [NSString stringWithCString:md5string length:CC_MD5_DIGEST_LENGTH*2];
	return [NSString stringWithCString:sha1string encoding:NSUTF8StringEncoding];
	
}

+ (NSString *)separateWithComma:(NSInteger)integer {
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithInteger:integer]];
    return formattedString;
}

+(NSString*)trim:(NSString*)s {
//	NSCharacterSet *seperator = [NSCharacterSet characterSetWithCharactersInString:@" \t\r\n\f"];
//	NSString *y = [s stringByTrimmingCharactersInSet:seperator];
	NSString *y = [s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	return y;
}
/*
+ (NSString *)stripTags:(NSString *)html {
	NSString *result = [html stringByReplacingOccurrencesOfRegex:@"<.*?>" withString:@""];
	result = [result stringByReplacingOccurrencesOfRegex:@"&nbsp;|[\r\n]+" withString:@" "];
	return result;
}

+ (NSString *)escapeHTML:(NSString *)html {
	return [self escapeHTML:html useBreak:NO];
}

+ (NSString *)toFlat:(NSString *)str {
	NSString *result = str;
	result = [self trim:result];
	result = [self unescapeHTML:result];
	result = [self stripTags:result];
	result = [result stringByReplacingOccurrencesOfRegex:@"[\\r\\n\\s　]+" withString:@" "];
	return result;
}

// useBreakがYESなら改行コードは<br>に
+ (NSString *)escapeHTML:(NSString *)html useBreak:(BOOL)useBreak {
	NSString *result = html;
	result = [result stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
	result = [result stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
	result = [result stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
	if (useBreak) {
		result = [result stringByReplacingOccurrencesOfRegex:@"\\r\\n|\\r|\\n" withString:@"<br />"];
	} else {
		result = [result stringByReplacingOccurrencesOfRegex:@"\\r\\n|\\r|\\n" withString:@"&nbsp;"];
	}
	result = [result stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
	return result;
}

+ (NSString *)unescapeHTML:(NSString *)escapedHtml {
	NSString *result = escapedHtml;
	result = [result stringByReplacingOccurrencesOfRegex:@"<br>" withString:@"\n"];
	result = [result stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
	result = [result stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
	result = [result stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
	result = [result stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
	return result;
}
+ (BOOL)isOnlyWhiteSpace:(NSString*)y {
	if (y.length < 1) return YES;
	return [y isMatchedByRegex:@"^[\\s\\t\\r\\n]*$" options:RKLDotAll inRange:NSMakeRange(0, y.length) error:nil];
 }
 */

+ (NSString *)uuid {
	return [[NSUUID UUID] UUIDString];
}

@end
